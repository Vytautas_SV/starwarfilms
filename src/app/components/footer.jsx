import React from 'react';
import styles from './footer.css';

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <p className="copyright">© 2018 Vytautas Paulauskas</p>
                </div>
            </footer>
        );
    }
}

export default Footer;
