import React from 'react';
import List from './body/list.jsx';
import FilmDetails from './body/filmdetails.jsx';
import ActorDetails from './body/actordetails.jsx';
import Cookie from '../cookie.jsx';
import styles from './body.css';

class Body extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 'homepage',
            pageid: null,
            filmsLoaded: null,
            filmsjson: null,
            filmsUnassociated: null,
            peopleLaoded: null,
            peoplejson: null,
            favoritefilm: null,
            favoriteactor: null
        };

        this.updatePageStates(false);
        this.favoriteSwich = this.favoriteSwich.bind(this);
        this.updateState = this.updateState.bind(this);
        this.requestJson = this.requestJson.bind(this);
        this.associateState = this.associateState.bind(this);
        this.favoriteSwich(null,"film","",false);
        this.favoriteSwich(null,"actor","",false);
    }

    favoriteSwich(e, type, _id, ismounted=true) {
        var cookieObj = new Cookie();
        var cookieVar = cookieObj.getCookie("fav-"+type);
        var favorites = cookieVar.split(",");
        if (!favorites)favorites=[];
        var index = favorites.indexOf(_id);
        if (index!==-1) {
            favorites.splice(index, 1);
        }
        else{
            favorites.push(_id);
        }
        var favstring = favorites.join(",");
        cookieObj.setCookie("fav-"+type,favstring,30);
        if(ismounted){
            var obj = {};
            obj["favorite"+type] = favorites;
            this.setState(obj);
        }
        else{
            this.state["favorite"+type]=favorites;
        }
    }

    componentDidMount() {
        this.updatePageStates = this.updatePageStates.bind(this);
        window.addEventListener("hashchange", this.updatePageStates, false);
        this.requestJson('filmsjson', 'filmsUnassociated', 'filmsLoaded', 'https://swapi.co/api/films/');
        this.requestJson('peoplejson', null, 'peopleLaoded', 'https://swapi.co/api/people/');
    }

    updatePageStates(ismounted = true) {
        var page = null;
        var pageid = null;
        if (window.location.hash) {
            var hashArray = window.location.hash.substring(1).split("/");
            page = hashArray[0];
            pageid = hashArray[1];
        }
        if (ismounted) {
            this.setState({
                page: page,
                pageid: pageid
            })
        }
        else {
            this.state = {
                page: page,
                pageid: pageid
            };
        }
    }

    associateState(stateName, copy) {
        if (this.state[stateName] && this.state[stateName].constructor === Array) {
            var newArray = [];
            var newArrayUnassociated = [];
            var addToArray = function(val, i) {
                var id = val.url.split("/").reverse()[1];
                val["_id"] = id;
                newArray["n"+id] = val;
                newArrayUnassociated.push(val);
            }
            this.state[stateName].forEach(addToArray);
            this.state[stateName] = newArray;
            this.state[copy] = newArrayUnassociated;
        }
    }

    updateState(stateName, stateVar) {
        var stateNameIsArray = false;
        if (this.state[stateName] && this.state[stateName].constructor === Array) {
            stateNameIsArray = true;
        }
        if (stateNameIsArray) {
            stateVar = stateVar.concat(this.state[stateName]);
        }
        if (stateNameIsArray) {
            this.state[stateName] = stateVar;
        }
        else {
            var obj = {};
            obj[stateName] = stateVar;
            this.setState(obj);
        }
    }

    requestJson(stateNameArray, stateNameUnassArray, stateNameStatus, url) {
        var requestJson = this.requestJson;
        var associateState = this.associateState;
        var updateState = this.updateState;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) { // readyState 4 means the request is done.
                if (xhr.status === 200) { // status 200 is a successful return.
                    var obj = JSON.parse(xhr.responseText);
                    updateState(stateNameArray, obj.results);
                    if (obj.next == null) {
                        associateState(stateNameArray, stateNameUnassArray);
                        updateState(stateNameStatus, true);
                    }
                    else {
                        requestJson(stateNameArray, stateNameUnassArray, stateNameStatus, obj.next);
                    }
                }
                else {
                    //TODO: deal with xhr errors
                }
            }
        }
        xhr.send(null);
    }

    renderer(component) {
        return(
            <main className="container">
                {component}
            </main>
        );
    }

    render() {
        var actors = null;
        if (this.state.peopleLaoded) {
            actors = this.state.peoplejson;
        }
        var films = null;
        if (this.state.filmsLoaded) {
            films = this.state.filmsjson;
        }
        switch (this.state.page) {
            case "film":
                return this.renderer (
                    <FilmDetails
                        films={films}
                        id={this.state.pageid}
                        actors={actors}
                        peopleLaoded={this.state.peopleLaoded}
                        favoriteSwich={this.favoriteSwich}
                        favoritefilm={this.state.favoritefilm}
                        favoriteactor={this.state.favoriteactor}
                    />
                );
            case "actor":
                return this.renderer (
                    <ActorDetails
                        id={this.state.pageid}
                        films={films}
                        actors={actors}
                        filmsLaoded={this.state.filmsLoaded}
                        favoriteSwich={this.favoriteSwich}
                        favoritefilm={this.state.favoritefilm}
                        favoriteactor={this.state.favoriteactor}
                    />
                );
            default:
                return this.renderer (
                    <List
                        list={this.state.filmsUnassociated}
                        favoriteSwich={this.favoriteSwich}
                        favoritefilm={this.state.favoritefilm}
                    />
                );
        }
    }
}

export default Body;
