import React from 'react';
import SearchBar from './list/searchbar.jsx';
import Row from './list/row.jsx';
import styles from './list.css';

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: ''
        };
        this.handleFilterTextInput = this.handleFilterTextInput.bind(this);
    };

    handleFilterTextInput(filterText) {
        this.setState({
            filterText: filterText
        });
    }

    orderFilmsByReleaseDate(a, b) {
        if (a.release_date < b.release_date)
            return -1;
        if (a.release_date > b.release_date)
            return 1;
        return 0;
    }

    render() {
        if (!this.props.list) {
            return (
                <div className="loader"></div>
            );
        }
        var list = this.props.list.sort(this.orderFilmsByReleaseDate);
        let i = 0;
        var rows = [];
        list.forEach((film) => {
            if (film.title.indexOf(this.state.filterText) === -1) {
                return;
            }
            rows.push(
                <Row
                film={film}
                favoritefilm={this.props.favoritefilm}
                favoriteSwich={this.props.favoriteSwich}
                key={i} />
            );
    		i++;
        });
        return (
            <div>
                <h1>Star Wars films list</h1>
                <SearchBar
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />
                <ul className="list">
                    {rows}
                </ul>
            </div>
        );
    }
}

export default List;
