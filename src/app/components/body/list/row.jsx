import React from 'react';
import styles from './row.css';

class Row extends React.Component {
    render() {
        var isfavorit="";
        if (this.props.favoritefilm.indexOf(this.props.film._id)!==-1) {
            isfavorit= "active";
        }
        return (
            <li className="row">
                <span className={"fav "+ isfavorit} onClick = {(e) => this.props.favoriteSwich(e, "film", this.props.film._id)}></span>
                <a href={"#film/"+this.props.film._id}>
                    <span className="title">{this.props.film.title}</span>
                    <span className="release-date">{this.props.film.release_date}</span>
                </a>
            </li>
        );
    }
}

export default Row;
