import React from 'react';
import Row from './list/row.jsx'

class ActorDetails extends React.Component {
  render(){
    if(!this.props.actors){
      return (
        <div className="loader"></div>
      );
    }
    var actorDetails=this.props.actors["n"+this.props.id];
    if(!actorDetails){
      return(
        <p className="not-found">Actor page not found</p>
      );
    }

    if(this.props.filmsLaoded){
      var filmsList = [];
      let i = 0;
      actorDetails.films.forEach((film) => {
        var key = "n"+film.split("/").reverse()[1];
        if(this.props.actors[key]){
          filmsList.push(
            <Row
              film = {this.props.films[key]}
              favoriteSwich={this.props.favoriteSwich}
              favoritefilm={this.props.favoritefilm}
              key={i}/>
          );
          i++;
        }
      });
    }
    else{
      filmsList = (<div className="loader"></div>);
    }
    var isfavorit="";
    if (this.props.favoriteactor.indexOf(actorDetails._id)!==-1) {
        isfavorit= "active";
    }

    return (
      <div>
        <h1>{actorDetails.name}<span className={"fav inline "+isfavorit} onClick = {(e) => this.props.favoriteSwich(e, "actor", actorDetails._id)}></span></h1>
        <div className="detailwrapper">
          <p className="detail-title">Height:</p>
          <p className="detail">{actorDetails.height}</p>
        </div>
        <div className="detailwrapper">
          <p className="detail-title">Mass:</p>
          <p className="detail">{actorDetails.mass}</p>
        </div>
        <div className="detailwrapper">
          <p className="detail-title">Hair:</p>
          <p className="detail">{actorDetails.hair_color}</p>
        </div>
        <div className="detailwrapper">
          <p className="detail-title">Gender:</p>
          <p className="detail">{actorDetails.gender}</p>
        </div>
        <div className="detailwrapper">
          <p className="detail-title">Films:</p>
          <ul className="list">
            {filmsList}
          </ul>
        </div>
      </div>
    );
  }
}

export default ActorDetails;
