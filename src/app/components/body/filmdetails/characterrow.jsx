import React from 'react';
import styles from './characterrow.css';

class CharacterRow extends React.Component {
    render() {
        var isfavorit="";
        if (this.props.favoriteactor.indexOf(this.props.characterId)!==-1) {
            isfavorit= "active";
        }
        return (
            <li className="row">
                <span className={"fav "+ isfavorit} onClick = {(e) => this.props.favoriteSwich(e, "actor", this.props.characterId)}></span>
                <a href={'#actor/'+this.props.characterId}>
                    {this.props.characterName}
                </a>
            </li>
        );
    }
}

export default CharacterRow;
