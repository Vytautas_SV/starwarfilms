import React from 'react';
import CharacterRow from  './filmdetails/characterrow.jsx';
import styles from './filmdetails.css';

class FilmDetails extends React.Component {
    render() {
        if (!this.props.films) {
            return (
                <div className="loader"></div>
            );
        }
        var filmdetails = this.props.films["n"+this.props.id];
        if (!filmdetails) {
            return (
                <p className="not-found">Film page not found</p>

            );
        }
        if (this.props.peopleLaoded) {
            var characterslist = [];
            let i = 0;
            filmdetails.characters.forEach((character) => {
                var key = "n"+character.split("/").reverse()[1];
                if (this.props.actors[key]) {
                    characterslist.push(
                        <CharacterRow
                            characterName={this.props.actors[key].name}
                            characterId={this.props.actors[key]._id}
                            favoriteSwich={this.props.favoriteSwich}
                            favoriteactor={this.props.favoriteactor}
                            key={i}
                        />
                    );
                    i++;
                }
            });
        }
        else {
            characterslist = (<div className="loader"></div>);
        }
        var isfavorit="";
        if (this.props.favoritefilm.indexOf(filmdetails._id)!==-1) {
            isfavorit= "active";
        }
        return(
            <div>
                <h1>{filmdetails.title}<span className={"fav inline "+isfavorit} onClick = {(e) => this.props.favoriteSwich(e, "film", filmdetails._id)}></span></h1>
                <div className="detailwrapper">
                    <p className="detail-title">Opening Crawl:</p>
                    <p className="detail">{filmdetails.opening_crawl}</p>
                </div>
                <div className="detailwrapper">
                    <p className="detail-title">Director:</p>
                    <p className="detail">{filmdetails.director}</p>
                </div>
                <div className="detailwrapper">
                    <p className="detail-title">Producer:</p>
                    <p className="detail">{filmdetails.producer}</p>
                </div>
                <div className="detailwrapper">
                    <p className="detail-title">Release Date:</p>
                    <p className="detail">{filmdetails.release_date}</p>
                </div>
                <div className="detailwrapper">
                    <p className="detail-title">Characters:</p>
                    <ul className="list characters">
                        {characterslist}
                    </ul>
                </div>
            </div>
        );
    }
}

export default FilmDetails;
