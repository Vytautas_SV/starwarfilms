import React from 'react';
import styles from './header.css';

class Header extends React.Component {
    render() {
        return (
            <header>
                <div className="container">
                    <div className="logo-wraper">
                        <a href="#" className="logo-wraper">
                            <span className="logo"></span>
                        </a>
                    </div>
                    <p className="quiz">HomeToGo Front-End Quiz</p>
                </div>
            </header>
        );
    }
}

export default Header;
