import React from 'react';
import {render} from 'react-dom';
import Footer from './components/footer.jsx';
import Header from './components/header.jsx';
import Body from './components/body.jsx';
import 'normalize.css';
import styles from './index.css';
require('file-loader?name=[name].[ext]!./index.html');

class Page extends React.Component {
    render() {
        var filmsList = null;
        if (this.props.films && this.props.films.results) {
            filmsList = this.props.films.results;
        }
        return (
            <div>
            <Header/>
            <Body filmsList={filmsList}/>
            <Footer/>
            </div>
        );
    }
}

render(
    <Page/>,
    document.getElementById('root')
);
